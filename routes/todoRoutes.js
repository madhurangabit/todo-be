const todoModel = require("../models/todoModel");

const response = require("../config/response");

const TodoRoutes = {
  getAllTodo: async (req, res) => {
    const { sortedBy } = req.query;
    try {
      const posts = await todoModel.find().sort({ endDate: sortedBy });
      res.json(posts);
    } catch (err) {
      return response.fail(req, res, response.message.server_error, null, err);
    }
  },

  createTodo: async (req, res) => {
    try {
      const { title, endDate, status } = req.body;
      const newPost = await todoModel.create({
        title,
        endDate,
        status,
      });
      res.json(newPost);
    } catch (err) {
      return response.fail(req, res, response.message.server_error, null, err);
    }
  },

  updateTodo: async (req, res) => {
    try {
      const { id } = req.params;
      const { title, status } = req.body;
      const post = await todoModel.findByIdAndUpdate(id, { title, status });
      res.json(post);
    } catch (err) {
      return response.fail(req, res, response.message.server_error, null, err);
    }
  },

  deleteTodo: async (req, res) => {
    try {
      const { id } = req.params;
      const post = await todoModel.findById(id);
      await post.remove();
      res.json("deleted");
    } catch (err) {
      return response.fail(req, res, response.message.server_error, null, err);
    }
  },
};
module.exports = TodoRoutes;
