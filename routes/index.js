const express = require('express');
const router = express.Router();


const todoRoutes = require('./todoRoutes');

router.get('/todo', todoRoutes.getAllTodo);
router.post('/todo', todoRoutes.createTodo);
router.put('/todo/:id', todoRoutes.updateTodo);
router.delete('/todo/:id', todoRoutes.deleteTodo);

module.exports = router;
