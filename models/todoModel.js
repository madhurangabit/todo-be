const mongoose = require("mongoose");
const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim:true,
    },
    endDate: {
      type: String,
      required: true,
      trim:true,
    },
    status: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  { timestamps: true }
);

const Post = mongoose.model("Post", schema);
module.exports = Post;
