const express = require("express");
const connection = require("./connection");
const response = require("./config/response");
const routes = require("./routes");
const todoModel = require("./models/todoModel.js");
const app = express();
const cors = require('cors');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(cors());

app.all('/*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'Content-type, Accept, Authorization, token'
  );
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    let reqData = {
      method: req.method,
      body: req.body,
      query: req.query,
      peram: req.peram,
    };
    console.log(req.url, reqData);
    next();
  }
});

app.use('/api/', routes);

app.use((req, res) => {
  return response.fail(req, res, response.message.invalid_url);
});

app.listen(3000, () => {
  console.log("Listening at port 3000");
});
